﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs241Test_1
{
    // Create an interface called IBookend that provides the 
    // following functions:
    //   -- A function to return true if the object has a bookend
    //   -- When the function returns true it also return the bookend char 
    //   -- via the out parameter.
    //   bool IsStartPoint(out char bookend)
    //  and the following property
    //   char TheChar;
    public interface IBookend
    {
        bool IsStartPoint(out char bookend);
        char TheChar { get; set; }
    }
}
