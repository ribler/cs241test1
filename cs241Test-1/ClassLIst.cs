﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs241Test_1
{
    public class ClassList : IEnumerable<string>
    {
        public ClassList()
        {
        }

        public void AddName(string name)
        {
            studentNames.Add(name);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return ((IEnumerable<string>)studentNames).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<string>)studentNames).GetEnumerator();
        }

        private List<string> studentNames = new List<string>();
    }
}
