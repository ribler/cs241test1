﻿namespace cs241Test_1
{
    public class BookendChar : IBookend
    {
        public BookendChar (char theBookendChar)
        {
            TheChar = theBookendChar;
        }
        public BookendChar (char theBookendChar, char theMatch)
        {
            TheChar = theBookendChar;
            matchingChar = theMatch;
        }

        public char TheChar { get; set; }

        private char matchingChar = '\0';

        public bool IsStartPoint(out char bookend)
        {
            bookend = matchingChar;
            return matchingChar != '\0';
        }
    }
}
