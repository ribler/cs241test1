﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs241Test_1
{
    public class Program
    {
        public static string reverse(string thingToReverse)
        {
            IEnumerator<char> enumerator = thingToReverse.GetEnumerator();
            return reverse(enumerator);
        }

        public static string reverse(IEnumerator<char> enumerator)
        {
            string returnValue = "";
            char thisChar;
            if (enumerator.MoveNext())
            {
                thisChar = enumerator.Current;
                if (thisChar != ')')
                {
                    if (thisChar == '(')
                    {
                        string subExpr = "(" + reverse(enumerator) + ")";
                        returnValue = reverse(enumerator) + subExpr;
                    }
                    else
                        returnValue = reverse(enumerator) + thisChar;
                }
            }
            return returnValue;
        }

        static void Main(string[] args)
        {
        }
    }
}
