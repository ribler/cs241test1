﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using cs241Test_1;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        // Bookend characters are characters that have a complimentary
        // character, such as the character "(" has a complimentary ")",
        // "]" has "[" and so one.  We can make other characters 
        // bookends arbitrarily.  I might say "a" has bookend 'z'.

        // Create an interface called IBookend that provides the 
        // following functions:
        //   -- A function to return true if the object has a bookend
        //   -- When the function returns true it also return the bookend char 
        //   -- via the out parameter.
        //   bool IsStartPoint(out char bookend)
        //  and the following property
        //   char TheChar;

        // Create a class called BookendChar that implements the 
        // IBookend interface.
        [TestMethod]
        public void createBookendChar()
        {
            IBookend myC = new BookendChar('c');
            char matchingChar;
            Assert.AreEqual('c', myC.TheChar);
            Assert.IsFalse(myC.IsStartPoint(out matchingChar));
        }

        [TestMethod]
        public void createBookendCharWithMatch()
        {
            IBookend myLeftParen = new BookendChar('(', ')');
            char matchingChar;
            Assert.AreEqual('(', myLeftParen.TheChar);
            Assert.IsTrue(myLeftParen.IsStartPoint(out matchingChar));
            Assert.AreEqual(')', matchingChar);
        }

        // Provide a recursive function to reverse a string.
        [TestMethod]
        public void reverseStringTest()
        {
            string reversed = Program.reverse("abcdefg");
            Assert.AreEqual("gfedcba", reversed);
        }

        //Modify the reverse program(or if you prefer, write another
        //function) that reverses things that are inside parenthesis.
        [TestMethod]
        public void reverseExpressionTest()
        {
            string reversed = Program.reverse("(a + b + c)");
            Assert.AreEqual("(c + b + a)", reversed);
        }


        //Modify the reverse program(or if you prefer, write another
        //function) that reverses nested expressions as shown in the
        //following test.
        [TestMethod]
        public void reverseNestedExpressionTest()
        {
            string reversed = Program.reverse("(a + (b + c) + d)");
            Assert.AreEqual("(d + (c + b) + a)", reversed);
        }

        [TestMethod]
        public void reverseVeryNestedExpressionTest()
        {
            string reversed = Program.reverse("(a + (b + (d * e) ) + (g + h) * i");
            Assert.AreEqual("(i * (h + g) + ( (e * d) + b) + a)", reversed);
        }

        //Modify the reverse program(or if you prefer, write another function)
        // that reverses the identifier names along with everything reversed
        // in the previous tests.
        [TestMethod]
        public void reverseNestedStringExpressionTest()
        {
            string reversed = Program.reverse("(abc + (bcd + (def * efg) ) + (ghi + hij) * ijk");
            Assert.AreEqual("(kji * (jih + ihg) + ( (gfe * fed) + dcb) + cba)", reversed);
        }

        [TestMethod]
        public void classListTest()
        {
            ClassList cs241 = new ClassList();
            cs241.AddName("firstStudent");
            cs241.AddName("secondStudent");
            string[] result = new string[] { "firstStudent", "secondStudent" };

            int resultIndex = 0;
            foreach(string name in cs241)
            {
                Assert.AreEqual(name, result[resultIndex++]);
            }
        }
    }
}
